function mapObject(obj, cb) {

    let result = Object(obj);

    for (let key in obj){

        if (obj[key] !== undefined && obj[key] !== null){
            result.key = (cb(obj[key]));
        }
    }
    delete result['key'];

    return result;
}
module.exports = {mapObject};