function invert(obj) {
    
    let keyArray = [];
    let valueArray = [];
    let myInvert = {};

    for (let key in obj){
       
        keyArray.push(key);
        valueArray.push(obj[key]);
    }

    keyArray.pop();
    valueArray.pop();

    for (let index = 0; index < valueArray.length; index++){
        myInvert[valueArray[index]] = keyArray[index];
    }
    
    return myInvert;
}

module.exports = {invert};