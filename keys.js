function keys(obj) {

    if (typeof obj !== "object"){
        return "Enter valid object";
    }
    let result=[];
    for(let key in obj){
        result.push(key);
    }
    return result;
}
module.exports={keys};

