Object.prototype.myDefault = function(obj, defaultProps){
    for (let key in obj){
        if (!obj[key]){
            obj[key] = defaultProps[key];
        }
    }
    delete obj.myDefault;
    return obj
}
function defaults(obj, defaultProps) {
    
    return myDefault(obj, defaultProps);
}
module.exports = {defaults};